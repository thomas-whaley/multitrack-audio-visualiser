# Audio Visualisation Programme

## DEPENDENCIES:
 - pygame version at least 1.9.5

## TO USE:

Run by using the command
```
python3 main.py
```

When asked to drag and drop a multitrack folder, please
drop one that contains this sort of thing:

```
directory_containing_mutitracks
    | - song_name-0.wav
    | - song_name-1.wav
    | - song_name-2.wav
    | - ...
    | - song_name-n.wav
    | - song_name.w
```

For example, if one wanted to put in Bohemian Rhapsody with 4 tracks
they would put:

```
Bohemian_Rhapsody:
    | - Bohemian_Rhapsody-1.wav
    | - Bohemian_Rhapsody-2.wav
    | - Bohemian_Rhapsody-3.wav
    | - Bohemian_Rhapsody-4.wav
    | - Bohemian_Rhapsody.wav
```

Make sure that only the relevant files are in the folder, or it may
go a bit crazy.

When in the visualisation, the up and down arrow keys can be pressed
to alter the amplitude of the drawing, and the escape key exits the
programme.

Have fun!

To use this application, make sure you have the latest version of Python 3 installed.

If you do not have Python 3 installed follow these steps:

Go to this website https://www.python.org/downloads/release/python-3100/ and scroll down to the bottom where it says Files.
Pick and install the version that is relevant to your operating system.

Now with Python 3 installed, make sure to have pygame installed.
Simply type 'pip3 install pygame' into the Command Prompt (Windows) or Terminal (macOS)

Make sure to read the guide to using the application in the main python file.
Run the main.py file by navigating to the directory in which the main file is located in the Command Prompt (Windows) or Terminal (macOS),
and then typing the 'python3 main.py' command into there.

Everything should work, although it may be a bit slow on some machines but we'll see!
