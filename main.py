"""
Audio Visualisation Programme

DEPENDENCIES:
 - pygame version at least 1.9.5

TO USE:
When asked to drag and drop a multitrack folder, please
drop one that contains this sort of thing:
Multitrack_Folder:
 - song_name-track_number.wav
 - ...
 - song_name.wav

For example, if one wanted to put in Bohemian Rhapsody with 4 tracks
they would put:
Bohemian_Rhapsody:
 - Bohemian_Rhapsody-1.wav
 - Bohemian_Rhapsody-2.wav
 - Bohemian_Rhapsody-3.wav
 - Bohemian_Rhapsody-4.wav
 - Bohemian_Rhapsody.wav

Make sure that only the relevant files are in the folder, or it may
go a bit crazy.

When in the visualisation, the up and down arrow keys can be pressed
to alter the amplitude of the drawing, and the escape key exits the
programme.

Have fun!
"""

import pygame
import math

from audio_vis import Audio
from multitrack import Multitrack
from window import Window
from DropFileScreen import DropFileScreen

pygame.init()


class Ring:
    def __init__(self, radius: float, center_pos: tuple, colour: list):
        """
        Represents a ring to be drawn. This ring uses polar coordinates
        (radius * (x * cos(theta) + y * sin(theta)) to make it easier
        for the program to draw the signal in a circle.
        :param radius: Starting radius
        :param center_pos: Center position
        :param colour: Colour
        """
        self.radius = radius
        self.center_pos = center_pos
        self.colour = colour

        # Define the number of points make up our circle
        # (the lower this number the faster the circle spins)
        self.num_points = 1000
        self.angle_per_increment = 360 / self.num_points
        self.increment = 0

        # List of radii at every point on the circle
        self.points = [radius for i in range(self.num_points)]

    def update(self, left: float, right: float, amplitude: int) -> None:
        """
        Method to update the ring given left and right sample data and the amplitude.
        :param left: Left channel sample data
        :param right: Right channel sample data
        :param amplitude: Amplitude of the displacement of the radius
        :return: None
        """
        # Increment the increment
        self.increment += 1
        self.increment %= self.num_points

        # Get the average displacement given the left and right signal
        average_displacement = (left + right) / 2

        # Update the radius at this particular angle to be the original radius offset by the average displacement
        self.points[int(self.increment)] = self.radius + average_displacement * amplitude

    def polar_to_rect(self, points: list) -> list:
        """
        Method to convert the polar coordinate to rectangle (Cartesian) coordinates
        :param points: List containing polar coordinate data
        :return: List containing rectangle coordinate data
        """
        rect = []
        for i in range(self.num_points):
            radius = points[i]
            angle = i * self.angle_per_increment
            x = radius * math.cos(math.radians(angle))
            y = radius * math.sin(math.radians(angle))
            rect.append([x + self.center_pos[0], y + self.center_pos[1]])
        return rect

    def render(self, window: pygame.Surface) -> None:
        """
        Render the ring to the given window.
        In this case we are simply connecting the points with lines
        :param window: Window
        :return: None
        """
        pygame.draw.aalines(window, self.colour, False, self.polar_to_rect(self.points))


class MultiAudioVis(Window):
    def __init__(self, multitrack: Multitrack):
        """
        Class to handle the visualization of the audio
        :param multitrack: Multitracks
        """
        super().__init__("Audio Visualiser", 0, 0, True)
        pygame.mouse.set_visible(False)

        self.multitrack = multitrack

        # Convert the sound objects to sample data objects
        self.audio_vis_multi = {track_number: Audio(sound) for track_number, sound in
                                self.multitrack.multitracks().items()}

        self.rings = self.make_rings()

        self.amplitude = 100


    def init(self) -> None:
        """
        Starts playing the music and starts updating the visualisation
        :return: None
        """
        self.multitrack.play()
        for track in self.audio_vis_multi.values():
            track.start(False)

    def make_rings(self) -> dict:
        """
        Method to initialize the rings
        :return: Dictionary of rings with track number as key
        """
        rings = {}
        smallest_size = min(self.width, self.height)
        # Space the rings out evenly
        for i in range(self.multitrack.num_tracks()):
            completion_percentage = ((i + 1) / self.multitrack.num_tracks()) * 0.9
            radius = smallest_size * completion_percentage * 0.5
            colour = [255 * completion_percentage, 255, 255 * (1 - completion_percentage)]
            rings[i + 1] = Ring(radius, (self.width / 2, self.height / 2), colour)
        return rings

    def get_audio_signal(self, track_num: int) -> list:
        """
        Gets the audio signal given a track number
        :param track_num: Track number
        :return: Audio signal
        """
        signal = self.audio_vis_multi[track_num].get(1)
        return signal

    def all_tracks_alive(self):
        """
        Checks if any of the tracks have finished
        :return: All checks are still alive
        """
        for track in self.audio_vis_multi.values():
            if track.has_finished():
                return False
        return True

    def handle_events(self) -> None:
        """
        Handle the events of the window
        :return: None
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
                # Up and down arrow keys control the amplitude
                if event.key == pygame.K_UP:
                    self.amplitude += 20
                if event.key == pygame.K_DOWN:
                    self.amplitude -= 20

    def render(self) -> None:
        """
        Handles the rendering of all of the rings
        :return: None
        """
        self.window.fill([0, 0, 0])
        if self.all_tracks_alive():
            for track_number, audio_vis_track in self.audio_vis_multi.items():
                signal = self.get_audio_signal(track_number)

                for point in signal:
                    self.rings[track_number].update(point[0], point[1], self.amplitude)
                    self.rings[track_number].render(self.window)

        pygame.display.flip()


if __name__ == '__main__':
    if pygame.version.vernum < (1, 9, 5):
        print("Pygame version out of date. Please update or reinstall to the latest version")
        quit()

    # Creates a new drop file screen
    drop_file_screen = DropFileScreen("Assets/Mohave-Light.ttf")
    # Gets the tracks from the user
    tracks = drop_file_screen.main()

    # If the user has given the correct data...
    if isinstance(tracks, Multitrack):
        # ...visualise it!
        multitrack_vis = MultiAudioVis(tracks)
        multitrack_vis.main()
