import pygame
import numpy
import time

pygame.init()


class Audio:
    def __init__(self, sound: pygame.mixer.Sound):
        """
        Gives a way to easily get sample information
        while a sound is being played. Normalizes the
        sample data to be between -1 and 1
        :param sound: Sound to be converted to samples
        """
        pygame.mixer.init()
        self.snd = sound
        self.snd_length = self.snd.get_length()

        self.start_time = time.time()

        self.arr = pygame.sndarray.array(self.snd)

        self.arr_length = len(self.arr)

        # Slow but we do it once
        self.max_value = numpy.amax(numpy.abs(self.arr))
        self.arr = numpy.divide(self.arr, self.max_value)

        self.ind = 0
        self.prev_ind = 0

    def start(self, play_music: bool = True) -> None:
        """
        Starts playing the music
        :param play_music: Play music
        :return: None
        """
        self.start_time = time.time()
        if play_music:
            self.snd.play()

    def get(self, range_index: int = 1) -> list:
        """
        Gets sample data at the current time
        :param range_index: How many samples ahead and behind do we get (Optional) (-1 for getting from the last call)
        :return: List of sample data
        """
        elapsed_time = time.time() - self.start_time
        self.ind = int((elapsed_time / self.snd_length) * self.get_arr_length())

        samples = []

        if range_index == -1:
            samples = self.arr[max(self.prev_ind, 0):min(self.ind, self.get_arr_length() - 1)]
        elif range_index <= 0:
            samples = [self.arr[max(min(self.ind, self.get_arr_length() - 1), 0)]]
        else:
            samples = self.arr[max(self.ind - range_index, 0):
                               min(self.ind + range_index, self.get_arr_length() - 1)]

        self.prev_ind = self.ind
        return samples

    def get_curr_index(self) -> int:
        """
        Returns the current index
        :return: Current index
        """
        return self.ind

    def get_arr_length(self) -> int:
        """
        Returns the length of the sample array
        :return: Length of sample array
        """
        return self.arr_length

    def get_max_value(self) -> float:
        """
        Returns the maximum value of the sample array pre
        normalization
        :return: Pre normalized maximum value
        """
        return self.max_value

    def has_finished(self) -> bool:
        """
        Returns true if the programme has finished
        :return: Has finished
        """
        return self.ind >= self.arr_length - 1
