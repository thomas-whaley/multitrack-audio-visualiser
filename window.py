import pygame


class Window:
    def __init__(self, title: str, width: int, height: int, fullscreen: bool = False, framerate: int = 0):
        """
        Basic application framework
        :param title: Title of application
        :param width: Width of application
        :param height: Height of application
        :param fullscreen: Fullscreen (Optional, Defaults to False)
        :param framerate: Framerate (Optional, Default is unbounded)
        """
        self.title = title
        self.fullscreen = fullscreen
        self.framerate = framerate
        # Setup a window
        self.window = pygame.display.set_mode((width, height), pygame.FULLSCREEN if fullscreen else 0)
        pygame.display.set_caption(self.title)
        self.width, self.height = self.window.get_size()

        # Timing stuff
        self.clock = pygame.time.Clock()
        self.elapsed_time = 1

        # Running flag decides when to quit the application
        self.running = True

    def init(self) -> None:
        """
        Method called when the application starts
        :return: None
        """
        pass

    def quit(self) -> None:
        """
        Method called when the application closes
        :return: None
        """
        pass

    def handle_events(self) -> None:
        """
        Method to handle the events such as quitting
        :return: None
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False

    def tick(self) -> None:
        """
        Update the clock
        :return: None
        """
        self.clock.tick(self.framerate)
        self.elapsed_time = self.clock.get_time()

    def render(self) -> None:
        """
        Render to the screen
        :return: None
        """
        self.window.fill([0, 0, 0])

        pygame.display.flip()

    def main(self) -> None:
        """
        Main loop
        :return:
        """
        self.init()
        while self.running:
            self.handle_events()
            self.tick()
            self.render()
        self.quit()
        pygame.quit()
