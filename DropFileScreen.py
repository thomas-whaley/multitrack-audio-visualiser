import os
import pygame

from window import Window
from multitrack import Multitrack


class DropFileScreen(Window):
    def __init__(self, font_path: str = ""):
        """
        Handle the screen for dropping a directory of multitracks
        """
        super().__init__("Load Multitracks", 1000, 600, False)

        # Initializes the font and text etc
        if font_path:
            self.font = pygame.font.Font(font_path, 36)
        else:
            self.font = pygame.font.SysFont("Arial", 36)
        self.font_surface = self.font.render("Drag and Drop a Multitrack Folder", True, [255, 255, 255])

        # Multitracks to return
        self.multitrack = None

    def on_receive_file(self, file_name: str) -> None:
        """
        Method is called when the user drops a file into the window
        :param file_name: Path to the file
        :return: None
        """
        self.multitrack = Multitrack(file_name)

    def handle_events(self) -> None:
        """
        Method to handle events like quitting and dropping files
        :return: None
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.running = False
            if event.type == pygame.DROPFILE:
                self.on_receive_file(event.file)

    def render(self) -> None:
        """
        Renders the text
        :return:
        """
        self.window.fill([0, 0, 0])
        self.window.blit(self.font_surface, (self.width / 2 - self.font_surface.get_width() / 2,
                                             self.height / 2 - self.font_surface.get_height() / 2))
        pygame.display.flip()

    def main(self) -> Multitrack:
        """
        Main method of the class. Returns the correctly initialized multitracks
        :return: Initialized multitracks
        """
        self.init()
        self.render()
        while self.running and self.multitrack is None:
            self.handle_events()
        self.quit()
        return self.multitrack
