from pygame import mixer
from os.path import splitext, sep
from os import listdir


class Multitrack:
    def __init__(self, file_name: str):
        """
        Represents a collection of tracks. Given a directory, this
        will separate multitrack files from the full mix file given
        that they are in the same directory. Expects multitracks to
        be of the form 'song_name-track_number.wav' and the full mix
        file to be of the form 'song_name.wav'.
        :param file_name: Directory containing multitracks
        """
        self.file_name = file_name

        # Initialise the pygame mixer so we can play audio
        mixer.init()

        # Split up the full mix from the multitracks
        self.allowed_file_types = ['.wav']

        self.track_names = [file for file in listdir(self.file_name)
                            if splitext(file)[-1] in self.allowed_file_types]

        self.full_mix_name = max(self.track_names)

        self.multitrack_names = {int(file.split("-")[-1].split(".")[0]): file for file in self.track_names
                                 if file is not self.full_mix_name}

        self.full_mix_sound = mixer.Sound(self.file_name + sep + self.full_mix_name)

        self.multitrack_sounds = {track_number: mixer.Sound(self.file_name + sep + file)
                                  for track_number, file in self.multitrack_names.items()}

    def num_tracks(self) -> int:
        """
        Returns the number of tracks contained in the multitrack object
        :return: Number of tracks
        """
        return len(self.multitrack_names)

    def multitracks(self) -> dict:
        """
        Returns a dictionary containing pygame Sound objects given a track number as a key
        :return: Sound objects
        """
        return self.multitrack_sounds

    def play(self) -> None:
        """
        Starts playing da music
        :return: None
        """
        self.full_mix_sound.play()
